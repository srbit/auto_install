#!/bin/bash

#Install wget
yum install wget -y

#Install JDK
echo "Install JDK 1.7u79"
mkdir /usr/java && cd /usr/java
wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/7u79-b15/jdk-7u79-linux-x64.tar.gz
tar -zxvf jdk-7u79-linux-x64.tar.gz

alternatives --install /usr/bin/java java /usr/java/jdk1.7.0_79/bin/java 2
alternatives --install /usr/bin/javac javac /usr/java/jdk1.7.0_79/bin/javac 2
alternatives --install /usr/bin/jar jar /usr/java/jdk1.7.0_79/bin/jar 2

#Install kafka
echo "Installing elasticsearch"
mkdir -p /opt/src && cd /opt/src
wget https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-1.7.4.tar.gz
tar -zxvf elasticsearch-1.7.4.tar.gz

echo "export JAVA_HOME=/usr/java/jdk1.7.0_79" >> ~/.bashrc
echo "export PATH=$PATH:$JAVA_HOME/bin" >> ~/.bashrc
echo "export /opt/auto_install"
