#!/bin/bash

#Install wget
yum install wget -y

#Install Nodejs
mkdir -p /opt/src && mkdir -p /opt/src/source && cd /opt/src
wget https://nodejs.org/download/release/latest-v0.12.x/node-v0.12.9-linux-x64.tar.gz
tar -zxf node-*.tar.gz
mv node-*.tar.gz /opt/src/source
cd node*

echo "export NODE_PATH=/opt/src/node-v0.12.9-linux-x64" >> ~/.bashrc
echo "export PATH=\$PATH:\$NODE_PATH/bin" >> ~/.bashrc
source ~/.bashrc

# Show version
node -v
npm -v
