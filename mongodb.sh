#!/bin/bash

#Install wget
yum install wget -y

#Install Nodejs
mkdir -p /opt/src && mkdir -p /opt/src/source && cd /opt/src
wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-rhel62-3.2.1.tgz
tar -zxf mongodb-*.tgz
mv mongodb-*.tar.gz /opt/src/source
cd mongodb*
mkdir -p data/db

echo "export MONGODB_PATH=/opt/src/mongodb-linux-x86_64-rhel62-3.2.1" >> ~/.bashrc
echo "export PATH=\$PATH:\$MONGODB_PATH/bin" >> ~/.bashrc
source ~/.bashrc

# Show version
mongod --version
