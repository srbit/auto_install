#!/bin/bash

#Install wget
yum install wget -y
#Install repo
rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm

#Instal supervisord
yum install supervisor -y
chkconfig supervisord on

#Install JDK
echo "Install JDK 1.7u79"
mkdir /usr/java && cd /usr/java
cp /home/centos/jdk-7u79-linux-x64.tar.gz /usr/java
tar -zxvf jdk-7u79-linux-x64.tar.gz

alternatives --install /usr/bin/java java /usr/java/jdk1.7.0_79/bin/java 2
alternatives --install /usr/bin/javac javac /usr/java/jdk1.7.0_79/bin/javac 2
alternatives --install /usr/bin/jar jar /usr/java/jdk1.7.0_79/bin/jar 2

#Install kafka
echo "Installing storm"
mkdir -p /opt/src && cd /opt/src
wget http://mirrors.viethosting.vn/apache/storm/apache-storm-0.9.6/apache-storm-0.9.6.tar.gz
tar -zxvf apache-storm-0.9.6.tar.gz

echo "export JAVA_HOME=/usr/java/jdk1.7.0_79" >> ~/.bashrc
echo "export PATH=$PATH:$JAVA_HOME/bin" >> ~/.bashrc
echo "export /opt/auto_install"
