#!/bin/bash

#Install wget
yum install wget -y

#Install repo
rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm

#Instal supervisord
yum install supervisor -y
chkconfig supervisord on

#Install JDK
echo "Install JDK 1.7u79"
mkdir /usr/java && cd /usr/java
wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/7u79-b15/jdk-7u79-linux-x64.tar.gz
tar -zxvf jdk-7u79-linux-x64.tar.gz

alternatives --install /usr/bin/java java /usr/java/jdk1.7.0_79/bin/java 2
alternatives --install /usr/bin/javac javac /usr/java/jdk1.7.0_79/bin/javac 2
alternatives --install /usr/bin/jar jar /usr/java/jdk1.7.0_79/bin/jar 2

#Install kafka
echo "Installing storm"
mkdir -p /opt/src && cd /opt/src
wget http://mirrors.viethosting.vn/apache/storm/apache-storm-0.9.6/apache-storm-0.9.6.tar.gz
tar -zxvf apache-storm-0.9.6.tar.gz

echo
"storm.zookeeper.servers:
    - "z1"
storm.local.dir: "/opt/src/apache-storm-0.9.6"
nimbus.host: "z1"
supervisor.slots.ports:
    - 6700
    - 6701
    - 6702
    - 6703
worker.childopts: "-Xmx1024m"
drpc.servers:
    - "z1"
#
storm.zookeeper.connection.timeout: 300000
storm.zookeeper.session.timeout: 300000
storm.messaging.netty.server_worker_threads: 1
storm.messaging.netty.client_worker_threads: 1
storm.messaging.netty.buffer_size: 5242880
storm.messaging.netty.max_retries: 100
storm.messaging.netty.max_wait_ms: 1000
storm.messaging.netty.min_wait_ms: 100" > /opt/src/apache-storm-0.9.6/conf/storm.yaml

echo "export JAVA_HOME=/usr/java/jdk1.7.0_79" >> ~/.bashrc
echo "export PATH=$PATH:$JAVA_HOME/bin" >> ~/.bashrc
echo "export /opt/auto_install"
