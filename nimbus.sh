#!/bin/bash

#Install wget
yum install wget -y

#Install repo
rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm

#Instal supervisord
yum install supervisor -y
chkconfig supervisord on

#Install JDK
echo "Install JDK 1.7u79"
mkdir /usr/java && cd /usr/java
wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/7u79-b15/jdk-7u79-linux-x64.tar.gz
tar -zxvf jdk-7u79-linux-x64.tar.gz

alternatives --install /usr/bin/java java /usr/java/jdk1.7.0_79/bin/java 2
alternatives --install /usr/bin/javac javac /usr/java/jdk1.7.0_79/bin/javac 2
alternatives --install /usr/bin/jar jar /usr/java/jdk1.7.0_79/bin/jar 2

#Install kafka
echo "Installing storm"
mkdir -p /opt/src && cd /opt/src
wget http://mirrors.viethosting.vn/apache/storm/apache-storm-0.9.6/apache-storm-0.9.6.tar.gz
tar -zxvf apache-storm-0.9.6.tar.gz

echo "
[program:nimbus]
command=/opt/src/apache-storm-0.9.6/bin/storm nimbus
autostart=true

[program:ui]
command=/opt/src/apache-storm-0.9.6/bin/storm ui
autostart=true

[program:drpc]
command=/opt/src/apache-storm-0.9.6/bin/storm drpc
autostart=true

[program:logviewer]
command=/opt/src/apache-storm-0.9.6/bin/storm logviewer
autostart=true" >> /etc/supervisord.conf

echo "export JAVA_HOME=/usr/java/jdk1.7.0_79" >> ~/.bashrc
echo "export PATH=$PATH:$JAVA_HOME/bin" >> ~/.bashrc
echo "export /opt/auto_install"
