#!/bin/bash

#Install wget
yum install wget -y

#Install JDK
echo "Install JDK 1.7u79"
mkdir /usr/java && cd /usr/java
wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/7u79-b15/jdk-7u79-linux-x64.tar.gz
tar -zxvf jdk-7u79-linux-x64.tar.gz

alternatives --install /usr/bin/java java /usr/java/jdk1.7.0_79/bin/java 2
alternatives --install /usr/bin/javac javac /usr/java/jdk1.7.0_79/bin/javac 2
alternatives --install /usr/bin/jar jar /usr/java/jdk1.7.0_79/bin/jar 2

yum install gcc -y
yum install jemalloc-devel -y
yum install tcl -y

#Install kafka
echo "Installing redis"
mkdir -p /opt/src && cd /opt/src
wget http://download.redis.io/releases/redis-3.0.6.tar.gz
tar -zxvf redis-3.0.6.tar.gz
cd /opt/src/redis-3.0.6
make distclean
make
make test

echo "sysctl -w net.core.somaxconn=65535" >> /etc/rc.local
echo "echo never > /sys/kernel/mm/transparent_hugepage/enabled" >> /etc/rc.local
echo 'vm.overcommit_memory = 1' >> /etc/sysctl.conf
echo never > /sys/kernel/mm/transparent_hugepage/enabled

echo "export REDIS_HOME=/opt/src/redis-3.0.6" >> ~/.bashrc
echo "export PATH=$PATH:$REDIS_HOME/src" >> ~/.bashrc
echo "export JAVA_HOME=/usr/java/jdk1.7.0_79" >> ~/.bashrc
echo "export PATH=$PATH:$JAVA_HOME/bin" >> ~/.bashrc
echo "export /opt/auto_install"
