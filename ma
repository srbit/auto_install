#!/bin/bash

echo never > /sys/kernel/mm/transparent_hugepage/enabled
echo never > /sys/kernel/mm/transparent_hugepage/defrag

/opt/src/mongodb-linux-x86_64-rhel62-3.2.1/bin/mongod \
--dbpath /opt/src/mongodb-linux-x86_64-rhel62-3.2.1/data/db \
> /opt/src/mongodb-linux-x86_64-rhel62-3.2.1/mongodb.out &
